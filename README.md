# Nightwatch Test Automation

This guide helps to understand the test automation framework

## Overview of Automation Framework
- Created using a browser automation tool called Nightwatch.js that simulates actions inside the browser.
- [Nightwatch.js]((https://nightwatchjs.org/api/)) is an integrated, easy to use End-to-End testing solution for web application and websites, written in Node.js using JavaScript as the programming language.
- It uses the standard W3C WebDriver API to drive browsers in order to perform commands and assertions on the Document Object Model (DOM) elements of a web application.
- Page Object Model structure model is followed for better reusability and maintainability. See [here](https://nightwatchjs.org/api/pageobject/) for details on how page object is implemented on Nighwatch.js. 
- Screenshots are captured for passed/failed verifications.
- Reports in the form of HTML file will be generated post-execution.
- GitLab is used as the source/version control.

### Tools Involved
| Tools | Name |
| :--- |:---|
| **Browser Automation Framework** | [Nightwatch.js](https://nightwatchjs.org/api/) (based on [WebDriver](https://www.selenium.dev/documentation/en/webdriver/)) |
| **Programming Language** | [JavaScript](https://www.w3schools.com/js/DEFAULT.asp) |
| **Runtime Environment** | [Node.js](https://nodejs.org/en/about/) |
| **Integrated Development Environment (IDE)** | [Visual Studio Code](https://code.visualstudio.com/) |

### Folder Structure
The test automation assets are all located inside `nightwatch-test/`. Below are the important files and folders:

- **common/** - location of user defined libraries/functions/methods.
- **page_objects/** - separated into 2 folders: `keyword_functions/` - location of page actions/keywords, `object_repositories/` - location of web element identifiers (xpath/css selectors).
- **properties/** - location of configuration management file for test execution (such is user logins, etc.).
- **test_data/** - location of test data specific for each test script.
- **tests/** - location of test scripts. Nightwatch.js will run anything under this folder.
- **html-reporter.js** - this JS module is responsible for generating the test report after execution (in the form of HTML).
- **nighwatch.conf.js** - this JS module is the driver of Nightwatch.js framework. It defines which folders the tests and page objects are located, browser configurations, etc.
- **package.json** - this JSON file is the heart of this Node.js project.
- **run-nightwatch.sh** - this shell script is the starting point of execution when running from deployment pipeline.

## How to Setup (for local development and execution)
1. Install `Node.js` on your local machine
2. Clone the repository from `git@gitlab.com:jscawaling/nightwatch-ui-test-automation.git`.
- Git knowledge is required. See [here](https://www.atlassian.com/git/tutorials/setting-up-a-repository) for instructions on how to use git commands
- An RSA token is required to SSH cloning. See [here](https://docs.gitlab.com/ee/ssh/) to for documentation on how to create RSA token for GitLab.
3. Change directory (`cd`) to `nightwatch-test/`.
4. Install everything from the `package.json` by running the following commands in order:
```
npx npm-check-updates -u
npm install
npm install chromedriver --detect_chromedriver_version
npm install geckodriver@latest
mkdir debug, logs, reports
```
4. Everything will be downloaded from NPM server and stored into `node_mdoules/`

## How to Execute
By the default, Nightwatch.js will execute all tests under `tests/` folder.

#### Execution Commands (to trigger execution manually)
1. Execute all tests (default command when running from deployment pipeline)
```
./node_modules/.bin/nightwatch --reporter ./html-reporter.js
```
2. Execute all tests from specific folder
```
./node_modules/.bin/nightwatch tests/01_Accounts --reporter ./html-reporter.js
```
3. Execute specific test
```
./node_modules/.bin/nightwatch tests/01_Accounts/TC01_Create_Person_Account.js --reporter ./html-reporter.js
```

### Automated Execution Flow
1. Run execution command
2. Look for `nightwatch.conf.js` module.
2. Look for the tests and page object folders defined in `nightwatch.conf.js`.
3. The environment variable `TEST_AUTO_SECRET` will then be parsed by `properties/config-file.js`. 
4. Execute the test scripts in `test/` folder.
5. Tests scripts referencing to page object will look for page object modules in `page_objects/`.
6. During execution, steps that has verifications (passed/failed) will capture screenshots in `reports/` folder and logs (detailed logs useful for debugging) will be stored in `logs/` folder.
7. After execution, an html report will be generated in `reports/` folder