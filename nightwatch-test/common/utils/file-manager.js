const fs = require('fs');
const path = require('path');
const log = require('../utils/logger');

const moduleName = path.basename(__filename, path.extname(__filename));

function createFile(path, data){
    try{
        fs.writeFileSync(path, data, 'utf8');

        log.info(moduleName, 'File created in [' + path + ']');
    }catch(err){
        log.error(moduleName, 'createFile: ' + err);
    }
}

function updateFile(path, data){
    try{
        const file = fs.readFileSync(path, 'utf8');
        let newData = data;

        fs.writeFileSync(path, newData, 'utf8');

        log.info(moduleName, 'File updated in [' + path + ']');
    }catch(err){
        log.error(moduleName, 'updateFile: ' + err);
    }
}

function checkFileExist(path){
    let blnRet = false;

    try{
        if(fs.existsSync(path)){
            blnRet = true;
        }
    }catch(err){
        log.error(moduleName, 'checkFileExist: ' + err);
    }

    return blnRet;
}

function exportFile(path, data){
    
    if(!checkFileExist(path)){
        createFile(path, data);
    }else{
        updateFile(path, data);
    }

}

module.exports = {
    exportFile
}