function formatDate(date, format){
    let retDate;

    let mm = ('0' + (date.getMonth() + 1)).slice(-2);
    let dd = ('0' + date.getDate()).slice(-2);
    let yyyy = date.getFullYear();

    switch(format.toLowerCase()){
        case 'mm/dd/yyyy':
            retDate = mm + '/' + dd + '/' + yyyy;
            break;
        case 'dd/mm/yyyy':
            retDate = yyyy + mm + dd;
            break;
        case 'yyyymmdd':
            retDate = yyyy + mm + dd;
            break;
        default:
            break;
    }

    return retDate;
}

function getCurrentDate(format){
    let date = new Date();
    let currentDate = formatDate(date, format);

    return currentDate;
}

function getTimestamp(){
    let timestamp;

    let tt = Math.floor(Date.now()/1000);
    timestamp = getCurrentDate('yyyymmdd') + '_' + tt;
    return timestamp;
}

function addToCurrentDate(format, daysToAdd){
    let sumDate;

    let date = new Date();
    date.setDate(date.getDate() + daysToAdd);
    
    sumDate = formatDate(date, format);
    return sumDate;
}

function subtractfFromCurrentDate(format, daysToSubtract){
    let diffDate;

    let date =  new Date();
    date.setDate(date.getDate() - daysToSubtract);
    
    diffDate = formatDate(date, format);

    return diffDate;
}

module.exports = {
    getCurrentDate,
    getTimestamp,
    addToCurrentDate,
    subtractfFromCurrentDate
}