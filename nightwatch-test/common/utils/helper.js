const log = require('.logger');

module.exports = {
    throw: {
        error: function(moduleName, message){
            log.error(moduleName, message);
            throw new Error(message);
        }
    }
}