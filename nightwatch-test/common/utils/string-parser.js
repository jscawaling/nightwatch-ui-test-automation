const path = require('path');
const log = require('../utils/logger');

const moduleName = path.basename(__filename, path.extname(__filename));

module.exports = {

    getText: function(string_text){
        let retVal = string_text;

        if(typeof retVal === 'undefined'){
            retVal = '';
        }

        return retVal;
    },

    getBoolean: function(string_text){
        let blnFlag = false;

        if(string_text.trim().toUpperCase() === 'TRUE'){
            blnFlag = true;
        }

        return blnFlag;
    },

    verifyEqual: function(string_text, string_to_compare){
        let blnFlag = false;

        let text = this.getText(string_text).trim().toLowerCase();
        let sub_text = this.getText(string_to_compare).trim().toLowerCase();

        if(text.localeCompare(sub_text) === 0){
            blnFlag = true;
        }

        return blnFlag;
    }



}
