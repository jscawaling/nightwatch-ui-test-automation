const path = require('path');
const log = require('./logger');

const moduleName = path.basename(__filename, path.extname(__filename));

module.exports = {
    randomString: function(length_of_string){
        let ans = '';
        let arr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        for(let i = length_of_string; i>0; i--){
            ans += arr[Math.floor(Math.random() * arr.length)];
        }

        return ans;
    },

    randomNumber: function(length_of_string){
        let ans = '';
        let arr = '0123456789';

        for(let i = length_of_string; i>0; i--){
            ans += arr[Math.floor(Math.random() * arr.length)];
        }

        return ans;
    },
}