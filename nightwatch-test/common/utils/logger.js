const log = require('log4js');

let dirname = __dirname;
dirname = dirname.replace('common/utils', '');

log.configure({
    appenders: {
        testLog: {
            type: 'file',
            filename: dirname + '/logs/test_run.log'
        },
        console: {
            type: 'console'
        }
    },

    categories: {
        default: {
            appenders: ['console', 'testLog'],
            level: 'trace'
        }
    }
});

function info(moduleName, message){
    const logger = log.getLogger(moduleName);
    logger.info(message);
}

function debug(moduleName, message){
    const logger = log.getLogger(moduleName);
    logger.debug(message);
}

function error(moduleName, message){
    const logger = log.getLogger(moduleName);
    logger.error(message);
}

module.exports = {
    info,
    debug,
    error,
}