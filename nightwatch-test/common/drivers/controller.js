const parse = require('../utils/string-parser');
const path = require('path');
const log = require('../utils/logger');
const screenshot = require('../../screenshots');

const moduleName = path.basename(__filename, path.extname(__filename));

const locator = {
    xp: 'xpath',
    css: 'css selector'
}

module.exports = {
    verifyPass: function(driver, message){
        log.info(moduleName, message);
        driver.verify.ok(true, message);
    },

    verifyFail: function(driver, message){
        log.error(moduleName, message);
        driver.verify.fail(message);
    },

    reload: function(driver){
        log.info('Reloading browser instance');
        driver.api.refresh();
    },

    clearValue: function(driver, selector){
        driver.getValue({locateStrategy: selector.locator, selector: selector.selector}, (result) => {
            const length = result.value.length;
            for (let i = 0; i < length; i++) {
                driver.keys(driver.Keys.RIGHT_ARROW);
            }
            for(let i=0; i<length; i++){
                driver.keys(driver.Keys.BACK_SPACE);
            }
          });
    },
    setValue: function(driver, selector, input_value){
        let text = parse.getText(input_value);

        if(text != ''){
            log.info(moduleName, 'Set [' + text + '] in element [' + selector.locator + '][' + selector.selector + ']');

            this.clearValue(driver, selector);
            driver
                // .clearValue({locateStrategy: selector.locator, selector: selector.selector})
                .setValue({locateStrategy: selector.locator, selector: selector.selector}, text);
        }
    },

    click: function(driver, selector){
        log.info(moduleName, 'Click element [' + selector.locator + '][' + selector.selector + ']');
        driver.click({locateStrategy: selector.locator, selector: selector.selector});
    },

    waitForElementVisible: function(driver, selector, message){
        log.info(moduleName, message);
        driver.verify.visible({locateStrategy: selector.locator, selector: selector.selector}, message);
        driver.pause(2000);
    },

    verifyElementVisible: function(driver, selector, message){
        log.info(moduleName, message);

        driver.waitForElementVisible(selector.locator, selector.selector, 60000, false, (result) => {
            driver.pause(2000);

            if(result.value){
                screenshot.takeScreenshot(driver, 'passed', message);
            }else{
                screenshot.takeScreenshot(driver, 'failed', message);
            }
        }, message);
    },

    switchToNewFrame: function(driver){
        log.info(moduleName, 'Switching to different frame');
        driver.frame(0);
    },

    switchToDefaultFrame: function(driver){
        log.info(moduleName, 'Switching to default frame');
        driver.frame(null);
    }
}