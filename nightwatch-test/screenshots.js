const fs = require('fs');
const dt = require('./common/utils/date-time');
const log = require('./common/utils/logger');
const parse = require('./common/utils/string-parser');
const path = require('path');

const moduleName = path.basename(__filename, path.extname(__filename));

module.exports = {
    // takeScreenshot: function(driver, filePath){
    //     driver.saveScreenshot(filePath);
    // },

    takeScreenshot: function(driver, status){
        let base_dir = './reports/screenshots';
        let group_dir = './reports/screenshots/' + driver.currentTest.group;
        let file_dir = './reports/screenshots/' + driver.currentTest.module;

        if(!fs.existsSync(base_dir)){
            fs.mkdirSync(base_dir);
            log.info(moduleName, 'New screenshot directory was created [' + base_dir + ']');
        }

        if(!fs.existsSync(group_dir)){
            fs.mkdirSync(group_dir);
            log.info(moduleName, 'New screenshot directory was created [' + group_dir + ']');
        }

        if(!fs.existsSync(file_dir)){
            fs.mkdirSync(file_dir);
            log.info(moduleName, 'New screenshot directory is created [' + file_dir + ']');
        }

        let file_name = '';
        file_name = file_dir + '/PASSED_' + dt.getTimestamp() + '.jpg';

        if(parse.verifyEqual(status, 'failed')){
            file_name = file_dir + '/FAILED_' + dt.getTimestamp() + '.jpg';
        }

        driver.saveScreenshot(file_name);
    },

    takeScreenshot: function(driver, status, message){
        let base_dir = './reports/screenshots';
        let group_dir = './reports/screenshots/' + driver.currentTest.group;
        let file_dir = './reports/screenshots/' + driver.currentTest.module;

        if(!fs.existsSync(base_dir)){
            fs.mkdirSync(base_dir);
            log.info(moduleName, 'New screenshot directory was created [' + base_dir + ']');
        }

        if(!fs.existsSync(group_dir)){
            fs.mkdirSync(group_dir);
            log.info(moduleName, 'New screenshot directory was created [' + group_dir + ']');
        }

        if(!fs.existsSync(file_dir)){
            fs.mkdirSync(file_dir);
            log.info(moduleName, 'New screenshot directory is created [' + file_dir + ']');
        }

        let file_name = '';
        file_name = file_dir + '/PASSED_' + dt.getTimestamp() + '_' + message + '.jpg';

        if(parse.verifyEqual(status, 'failed')){
            file_name = file_dir + '/FAILED_' + dt.getTimestamp() + '_' + message + '.jpg';
        }

        driver.saveScreenshot(file_name);
    }
}
