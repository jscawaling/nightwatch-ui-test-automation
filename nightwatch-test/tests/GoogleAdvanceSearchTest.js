const log = require('../common/utils/logger');
const path = require('path');
const {config} = require('../properties/config-file');
const moduleName = path.basename(__filename, path.extname(__filename));

module.exports = {
    beforeEach: function(browser){
        log.info(moduleName, '************************** Initialize test [' + moduleName + ']' + '**************************');
        browser.windowMaximize();
    },

    'GoogleAdvanceSearchTest': function(browser){
        const google_advance_search_page = browser.page.GoogleAdvanceSearchPage();

        browser
            .url('https://www.google.com/advanced_search')
            .perform(function(){
                google_advance_search_page.searchText('test')
            })
    },

    afterEach: function(browser, done){
        log.info(moduleName, 'CLEAN UP');
        browser.end();
        done();
    }
}
