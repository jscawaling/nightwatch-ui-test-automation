const dt = require('./common/utils/date-time')

let htmlReporter = require('nightwatch-html-reporter');

let report_dir = './reports/';

let reporter = new htmlReporter({
    openBrowser: false,
    reportsDirectory: report_dir,
    reportFilename: 'report_' + dt.getTimestamp(),
});

module.exports = {
    write: function(results, options, done){
        reporter.fn(results, done);
    }
}
