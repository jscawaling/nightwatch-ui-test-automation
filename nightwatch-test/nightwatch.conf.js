const chromedriver = require('chromedriver');
const {pageObjectPath} = require('./page_objects/page-object-manager');

module.exports = {
    src_folders: ['tests'],
    page_objects_path: pageObjectPath,
    
    test_workers: {
        enabled: false,
        workers: 'auto',
    },

    screenshots: {
        enabled: true,
        path: './reports/screenshots/',
        on_failure: true,
        on_error: true,
    },

    test_settings: {
        default: {
            end_session_on_fail: true,
            skip_testcases_on_fail: false,
            globals: {
                retryAssertionTimeout: 60000,
                waitForConditionPollInterval: 5000,
                waitForConditionTimeout: 60000,
                asyncHookTimeout: 180000,
                abortOnAssertionFailure: false
            },
            webdriver: {
                start_process: true,
                server_path: chromedriver.path,
                port: 4444,
                cli_args: ['--port=4444'],
            },
    
            desiredCapabilities: {
                browserName: 'chrome',
                chromeOptions: {
                    args: [
                        'headless',
                        'disable-web-security',
                        'ignore-certificate-errors',
                        'window-size=1920,1400',
                        'no-sandbox',
                        'disable-dev-shm-usage',
                        'disable-setuid-sandbox',
                        'disable-notifications'
                    ]
                }
            }
        }
    }
}