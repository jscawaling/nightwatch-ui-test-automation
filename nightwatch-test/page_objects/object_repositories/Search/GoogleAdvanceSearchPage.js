const elements = {
    find_with_pages: {
        textbox: {
            all_these_words: {locator: 'xpath', selector: "//input[@name='as_q']"}
        }
    },

    then_narrows_your_results_by: {
        button: {
            advanced_search: {locator: 'xpath', selector: "//input[@value='Advanced Search']"}
        }
    }
}

module.exports.elements = elements;