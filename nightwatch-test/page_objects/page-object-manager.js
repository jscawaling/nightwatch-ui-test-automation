const fs = require('fs');
const path = require('path');
const { test_settings } = require('../nightwatch.conf');

let dirName = __dirname;
dirName.replace('');

const getAllFolders = function(dirPath, arrayOfFiles){
    const files = fs.readdirSync(dirPath)

    arrayOfFiles = arrayOfFiles || []
    
    files.forEach(function (file) {
        if(fs.statSync(dirPath + '/' + file).isDirectory()) {
            arrayOfFiles = getAllFolders(dirPath + '/' + file, arrayOfFiles)
            arrayOfFiles.push(path.join(dirPath, '/', file))
        }
    })

    return arrayOfFiles;
}

let allPageObjectPath = getAllFolders(
    path.join(__dirname, '/keyword_functions')
)

module.exports = {
    pageObjectPath: allPageObjectPath
}