const base = require('../../../common/drivers/controller');
const path = require('path');
const parse = require('../../../common/utils/string-parser');
const {elements} = require('../../object_repositories/Search/GoogleAdvanceSearchPage');

module.exports = {
    commands: [{
        searchText: function(text_to_search){
            this
                .perform(function(){
                    base.verifyElementVisible(this, elements.find_with_pages.textbox.all_these_words, 'Verify Google Advance Search page is displayed');
                    base.setValue(this, elements.find_with_pages.textbox.all_these_words, text_to_search);        
                    base.click(this, elements.then_narrows_your_results_by.button.advanced_search);
                    // this.pause(5000);

                })

            return this;
        }
    }]
}